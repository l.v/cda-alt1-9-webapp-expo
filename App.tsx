import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FontAwesome } from "@expo/vector-icons";

import { Routes, stackTyping } from './src/navigation';
import Calendar from "./src/views/Calendar";
import Scanner from "./src/views/Scanner";
import Search from "./src/views/Search";
import EntriesDay from "./src/views/EntriesDay";

const { Navigator: Stack, Screen } =  createBottomTabNavigator<stackTyping>();

const initialRoute = Routes.CALENDAR;
const routes = [
  {name: Routes.CALENDAR, component: Calendar, icon: "calendar"},
  {name: Routes.SCANNER, component: Scanner, icon: "barcode"},
  {name: Routes.SEARCH, component: Search, icon: "search"},
  {name: Routes.ENTRIESDAY, component: EntriesDay, icon: "list"},
];

const makeScreen = ({name, icon, component}: {name: Routes, icon: any, component: any}) => (
  <Screen
    options={{
      tabBarIcon: ({ focused }) => (
        <FontAwesome
          name={icon}
          size={24}
          color={focused ? "blue" : "black"}
        />
      ),
      tabBarLabel: name,
    }}
    name={name}
    component={component}
  />
);



export default () => {
  return (
    <SafeAreaView style={styles.app}>
      <NavigationContainer>
        <Stack initialRouteName={initialRoute}>
          { routes.map(makeScreen) }
        </Stack>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    margin: 10
  }
});
