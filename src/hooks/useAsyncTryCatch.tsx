import { useState, useEffect } from 'react';

import useAsyncEffect from './useAsyncEffect';


export type FailableAsyncHookReturn <T> = {
  isLoading: boolean,
  isError: boolean,
  result: T | null
}

// snack doesn't like generics, it thinks they're components
export default /*<T>*/(func: Function, deps: any[]): FailableAsyncHookReturn< /*T*/ any> => {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [result, setResult] = useState(null);

  useAsyncEffect(async () => {
    try {
      const res = await func();
      setResult(res);
      setIsLoading(false);
    } catch (error) {
      setIsError(true);
      setIsLoading(false);

      console.error(error);
    }
  }, deps);

  return {isLoading, isError, result};
}
