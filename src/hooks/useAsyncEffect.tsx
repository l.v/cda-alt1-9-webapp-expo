import { useState, useEffect } from 'react';

export default (effect: Function, deps?: any[]) => useEffect(() => { effect() }, deps);
