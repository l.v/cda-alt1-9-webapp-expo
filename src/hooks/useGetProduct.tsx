import { useState, useEffect } from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";

import { getProductByCode } from '../api/OpenFoodFact';
import { addFoodData, getFoodData } from '../services/foodDataCache';
import useAsyncTryCatch, { FailableAsyncHookReturn } from './useAsyncTryCatch';
import cleanData from '../services/cleanOFFProductData';
import { CleanedProductData } from '../types/CleanedProductData';


export default (code: string): FailableAsyncHookReturn<CleanedProductData> => {
  return useAsyncTryCatch(async () => {
    const cached = await getFoodData(code);
    console.log("cached result", cached)

    if (cached !== null) {
      return cached;
    }

    console.log("fetching product...")
    const productData = await getProductByCode(code);
    const cleanedProductData = cleanData(productData.product);

    await addFoodData(code, cleanedProductData);
    return cleanedProductData;
  }, [code]);
};

