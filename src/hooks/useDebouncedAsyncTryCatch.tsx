import { useState, useEffect, useCallback } from "react";

export type FailableAsyncHookReturn <T> = {
  isLoading: boolean,
  isError: boolean,
  result: T | null
}

// snack doesn't like generics, it thinks they're components
export default /*<T>*/(effect: Function, deps: any[] = [], delay = 250): FailableAsyncHookReturn< /*T*/ any> => {
  const callback = useCallback(effect, deps);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [result, setResult] = useState(null);

  useEffect(() => {
    const timeout = setTimeout(async () => {
      try {
        const res = await callback();
        setResult(res);
        setIsLoading(false);
      } catch (error) {
        setIsError(true);
        setIsLoading(false);

        console.error(error);
      }
    }, delay);
    return () => clearTimeout(timeout)
  }, [...deps, callback, delay]);

  return {isLoading, isError, result};
}
