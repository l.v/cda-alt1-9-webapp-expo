import { useState, useEffect } from 'react';

import useAsyncEffect from './useAsyncEffect';

const usePermission = (getPermissionFn: Function) => {
  const [hasPermission, setHasPermission] = useState(false);
  
  useAsyncEffect(async () => {
    const { status } = await getPermissionFn();
    setHasPermission(status === 'granted');
  });

  return { hasPermission };
}
