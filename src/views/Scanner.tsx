import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Image, TextInput } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

import OFFFood from '../components/OFFFood';
import usePermission from '../hooks/usePermission';



export default () => {
  //temporary hardcoded values because web doesn't handle BarCodeScanner
  const [scanned, setScanned] = useState(true);
  const [scannedData, setScannedData] = useState("3017620422003");
  const hasPermission = true;
  //TODO: replace with these
  // const [scanned, setScanned] = useState(false);
  // const [scannedData, setScannedData] = useState(null);
  // const { hasPermission } = usePermission(BarCodeScanner.requestPermissionsAsync);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    setScannedData(data);
  };

  if (hasPermission === null)
    return <Text>Requesting for camera permission</Text>;

  if (hasPermission === false)
    return <Text>No access to camera</Text>;

  if (scanned === true)
    return (
      <View>
        <OFFFood code={ scannedData } />
        <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />
      </View>
    );
  else
    return (
      <View>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
      </View>
    );
}