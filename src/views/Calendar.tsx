import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text, Button } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Calendar } from "react-native-calendars";

// TODO

export default () => {
  const [ selectedDate, setSelectedDate ] = useState(Date());
  const handleDayPress = (date) => {
    console.log(date);
    setSelectedDate(date.dateString);
  }

  const vacation = {key:'vacation', color: 'red', selectedDotColor: 'blue'};
  const massage = {key:'massage', color: 'blue', selectedDotColor: 'blue'};
  const workout = {key:'workout', color: 'green'};

  return (
    <SafeAreaView style={styles.app}>
      <Button title="today" onPress={ () => setSelectedDate(Date()) } />
      <Calendar
        current={ selectedDate }
        onDayPress={ handleDayPress }
        markingType={'multi-dot'}
        markedDates={{
          '2021-06-25': {dots: [vacation, massage, workout]},
          '2021-06-22': {dots: [massage, workout]}
        }}
      />
      <Text>{selectedDate}</Text>
      <Button title={'Delete cache'} onPress={async () => {
        const keys = await AsyncStorage.getAllKeys();
        AsyncStorage.multiRemove(keys.filter(key => key.startsWith("OFF_")))
      }} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    margin: 10
  }
});
