import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text, Button, View } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";

import useAsyncEffect from '../hooks/useAsyncEffect';
import { getFoodData } from '../services/foodDataCache';
import { getFood } from '../services/foodStorage';
import ConsumedFood from '../components/ConsumedFood';
import CleanedProductData from '../types/CleanedProductData';

//TODO: proper type merge, I don't think this is the right way
type MergedFoodData = ConsumedFood & CleanedProductData;

//TODO: this doesn't refresh when you add consumed foods
//TODO: get key for today or from the calendar
export default () => {
  const key = "2021-06-11TODOTODO";
  const [ foods, setFoods ] = useState<MergedFoodData[]>([]);

  useAsyncEffect(async () => {
    const foodForDay = await getFood(key);
    
    const foodsDataPromises = foodForDay.map(async food => ({
      ...food,
      ...await getFoodData(food.foodId),
    }));
    const foodsData = await Promise.all(foodsDataPromises);

    setFoods(foodsData);
  }, [key]);


  const totalkCals = foods.reduce(
    (acc, curr) => acc += Math.floor(curr.energyKcal100g / 100 * curr.amountg)
    , 0
  );
  return (
    <SafeAreaView>
      { foods.map(food => <ConsumedFood data={food} />) }
      <Text>Total: {totalkCals} Kcal</Text>
    </SafeAreaView>
  );
}
