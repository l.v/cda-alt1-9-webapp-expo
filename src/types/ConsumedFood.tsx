type ConsumedFood = {
  date: string,
  foodId: string,
  amountg: number,
}

export default ConsumedFood;