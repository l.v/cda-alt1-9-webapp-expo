import AsyncStorage from "@react-native-async-storage/async-storage";

import CleanedProductData from '../types/CleanedProductData';



export const getFoodData = async (id: string): Promise<CleanedProductData | null> => {
  const key = `OFF_${id}`;
  const storedStr = await AsyncStorage.getItem(key);

  return storedStr ? JSON.parse(storedStr) : null;
}


export const addFoodData = async (id: string, data: CleanedProductData) => {
  const key = `OFF_${id}`;
  console.log("setting", key, data)

  await AsyncStorage.setItem(key, JSON.stringify(data));
}
