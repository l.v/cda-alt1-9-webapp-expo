//TODO: rename to "consumedFoodStorage"

import AsyncStorage from "@react-native-async-storage/async-storage";

import ConsumedFood from '../types/ConsumedFood';


export const getFood = async (date: string): Promise<ConsumedFood[]> => {
  const key = `food_${date}`;
  const storedStr = await AsyncStorage.getItem(key);

  return storedStr ? JSON.parse(storedStr) : [];
}


export const addFood = async (date: string, foodId: string, amountg: number) => {
  const key = `food_${date}`;

  const stored = await getFood(date);
  stored.push({ date, foodId, amountg });
  
  await AsyncStorage.setItem(key, JSON.stringify(stored));
}
