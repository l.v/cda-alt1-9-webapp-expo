import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Image, TextInput } from 'react-native';

export default (props) => (
  <TextInput
    style={ {
      height: 40,
      margin: 12,
      borderWidth: 1,
    } }
    { ...props }
  />
);