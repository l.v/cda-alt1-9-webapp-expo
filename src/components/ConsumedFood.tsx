import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text, Button, View } from "react-native";

export default ({data}) => {
  return <View>
    <Text>{ data.name }: { Math.floor(data.energyKcal100g / 100 * data.amountg) }Kcals </Text>
  </View>
}