import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Image } from 'react-native';

import TextInput from './TextInputOneLine';
import useGetProduct from '../hooks/useGetProduct';
import { addFood } from '../services/foodStorage';


type Props = { code: string };

export default ({code}: Props) => {
  const { isLoading, isError, result } = useGetProduct(code);
  const [ consumedAmountg, setConsumedAmountg ] = useState(0);

  if (isLoading) return <View><Text>Loading...</Text></View>;

  // TODO (lol) display error message
  if (isError) return <View><Text>Error fetching product</Text></View>;

  if (!result) return null; //This will never happen, but I'm bad at typing things

  // Not very good
  if (consumedAmountg === 0 && result.defaultServingGrams)
    setConsumedAmountg(result.defaultServingGrams);

  return <View>
    <Text style={ {textAlign: "center", fontWeight: "bold"}}>{ result.name }</Text>
    <Image style={{width: 50, height: 50}} source={ { uri: result.imageUrl } } />
    <Text>Energy: { result.energyKcal100g }Kcal / 100g</Text>
    <View
        style={ {
          display: "flex", textAlign: "center"
          }
        }
      >
      <Text>Amount consumed:</Text>
      <TextInput
        keyboardType="decimal-pad"
        value={ consumedAmountg.toString() || 0 }
        onChangeText={ (value: string) => setConsumedAmountg(parseFloat(value)) }
      />
    </View>
      <Button title={'Tap to add'} onPress={() => addFood("2021-06-11TODOTODO", code, consumedAmountg) } />
  </View>
};